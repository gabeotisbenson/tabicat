# Tabicat
Tabicat is a simple little website for viewing guitar tabs and chords.  It uses
Vue under the hood, and is bundled up using ParcelJS.

If you notice any bugs, feel free to create a ticket under issues.  If you'd
like to contribute, get in touch with me!  I primarily made this for myself to
enjoy so it's a bit tailored (or perhaps un-tailored) to my needs/wants.  But,
that doesn't mean I'm unwilling to improve upon it and make it better for a
community should one develop.

All of my code is GPL-3.0, so hack away!
