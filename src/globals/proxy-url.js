let proxyUrl = 'http://localhost:8081/';

if (process.env.NODE_ENV === 'production') proxyUrl = 'https://cors.gabe.xyz/';

export default proxyUrl;
