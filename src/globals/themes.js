export const themes = [
	{ name: 'Light', isDefault: true },
	{ name: 'Dark', slug: 'dark' }
];
